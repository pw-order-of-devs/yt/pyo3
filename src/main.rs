use pyo3::prelude::*;
use pyo3::wrap_pyfunction;

///
/// Rust 2 Python
///

#[pyfunction]
fn add(_py: Python, a: i64, b: i64) -> PyResult<i64> {
    Ok(a + b)
}
#[pyfunction]
fn sub(_py: Python, a: i64, b: i64) -> PyResult<i64> {
    Ok(a - b)
}

#[pyfunction]
fn subfunction() -> String {
    "Subfunction".to_string()
}

#[pyclass]
struct SomePyClass {
    name: String,
    value: i64,
    flag: bool,
}

#[pymodule]
fn rust2py(py: Python, m: &PyModule) -> PyResult<()> {
    #[pyfn(m, "sum_as_string")]
    fn hello(_py: Python, string: &str) -> PyResult<String> {
        let out = String::from(format!("Hello, {}", string));
        Ok(out)
    }

    m.add_function(wrap_pyfunction!(add, m)?)?;
    m.add_function(wrap_pyfunction!(sub, m)?)?;

    let submod = PyModule::new(py, "submodule")?;
    submod.add_function(wrap_pyfunction!(subfunction, submod)?)?;
    m.add_submodule(submod)?;

    m.add_class::<SomePyClass>()?;

    Ok(())
}

///
/// Python to Rust
///

#[derive(FromPyObject)]
struct SomeRustStruct {
    #[pyo3(attribute("name"))]
    key: String,
    #[pyo3(item("value"))]
    data: i64,
    flag: bool,
}

fn main() {
    let gil = Python::acquire_gil();
    let py = gil.python();

    // PyModule::import allows accessing python api
    let builtins = PyModule::import(py, "builtins").unwrap();
    let sum1: i32 = builtins.call1("sum", (vec![1, 2, 3],)).unwrap().extract().unwrap();
    assert_eq!(sum1, 6);

    // py.eval evaluates single expression
    let sum2: i32 = py.eval("1 + 2 + 3", None, None).unwrap().extract().unwrap();
    assert_eq!(sum2, 6);
    let arr: Vec<f64> = py.eval("[1., 2., 3.]", None, None).unwrap().extract().unwrap();
    assert_eq!(arr, vec![1., 2., 3.]);

    // Python::run calls a statement
    Python::run(py, "assert 2 == 2", None, None);

    // PyModule::from_code generates module from file or snippet
    let asserts = PyModule::from_code(py, r#"
def assert_gt(a, b):
    assert a > b
def assert_le(a, b):
    assert a <= b
    "#, "asserts.py", "asserts").unwrap();
    asserts.call1("assert_gt", (4, 2,)).unwrap();
}
